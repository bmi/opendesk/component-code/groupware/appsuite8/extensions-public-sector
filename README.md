# Public Sector Integrations for OX App Suite

This repository contains OX App Suite integrations with external systems used in the Sovereign Workplace environment.

## Look and Feel

To have a unified look and feel across the entire Sovereign Workplace of applications, this integration provides a set of default settings for `open-xchange-dynamic-theme`. For details, see [the documentation](documentation/ui/011_look_and_feel.md).

## Central Navigation

To navigate between different applications in the Sovereign Workplace, the contents of the navigation menu is configured centrally. This integration uses that configuration to display the navigation menu. For details, see [the documentation](documentation/ui/012_central_navigation.md).

## Element for MAV

The Messaging, Audio and Video (MAV) integration ads Element rooms as conferences to appointments in OX App Suite. For details, see [the documentation](documentation/ui/017_video_conference.md).
