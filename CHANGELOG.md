# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.0.1] - 2023-08-31
- Fixed navigation menu after opening enterprise picker

## [2.0.0] - 2023-08-30
- Removed local copy of logo.svg, we are using a remote OpenDesk logo now
- PBSR-781: Missing error icon when adding video conference
- Work around OXUIB-2522: Enterprise picker sorts names backwards
- Updated launcher for changes in 8.16
- Changed color of the topbar border to #adb3bc
- PBSR-665: Allow selective enabling of PublicSector features

## [1.0.3] - 2023-08-04
- OXUI-1291: Publish parts of the code under a different license
- PBSR-815: Video Conference integration does not work in 8.15

## [1.0.2] - 2023-05-17
- Fixed conference logo in appointment edit dialog

## [1.0.1] - 2023-05-17
- PBSR-640: Upgrade central navigation to OX 8

## [1.0.0] - 2023-05-09
- PBSR-576: Ported customizations to OX 8

## [0.0.1]

[unreleased]: https://gitlab.open-xchange.com/extensions/public-sector/compare/2.0.1...main
[2.0.1]: https://gitlab.open-xchange.com/extensions/public-sector/compare/2.0.0...2.0.1
[2.0.0]: https://gitlab.open-xchange.com/extensions/public-sector/compare/1.0.3...2.0.0
[1.0.3]: https://gitlab.open-xchange.com/extensions/public-sector/compare/1.0.2...1.0.3
[1.0.2]: https://gitlab.open-xchange.com/extensions/public-sector/compare/1.0.1...1.0.2
[1.0.1]: https://gitlab.open-xchange.com/extensions/public-sector/compare/1.0.0...1.0.1
[1.0.0]: https://gitlab.open-xchange.com/extensions/public-sector/compare/middleware-public-sector-8.12.2...1.0.0
